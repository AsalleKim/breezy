#include "global.h"

void get_interfaces (void)
{
    int x, y;
    getmaxyx(stdscr, y, x);
    if_window = tui_new_shadowed_win(6, x/4, y/1.5, x/2, "Menu");
    wprintw(if_window->overlay, "Make a choice with a mouse or \'b\',\'h\',\'q\'");
    top_panel(if_window->panel);
    touchwin(panel_window(if_window->panel));
    update_panels( );
    doupdate( );
}

void getStation(int stationNumber, char *s)
{
	switch(stationNumber)
	{
		case 1: strcpy(s, "Odesa");
			break;
		case 2: strcpy(s, "Illichivsk");
			break;
		case 3: strcpy(s,"Poti");
			break;
		case 4: strcpy(s, "New York");
			break;
		case 5: strcpy(s, "Bergen");
			break;
		case 6: strcpy(s, "Murmansk");
			break;
		case 7: strcpy(s, "Anchorage");
			break;
		case 8: strcpy(s, "Victoria");
			break;
		case 9: strcpy(s, "Acapulco");
			break;
		case 10:strcpy(s, "Portland");
			break;
		case 11:strcpy(s, "New Orleans");
			break;
		case 12:strcpy(s,"La Libertad");
			break;
		case 13:strcpy(s,"Nazea");
			break;
		case 14:strcpy(s, "Bahia Blanca");
			break;
		case 15:strcpy(s, "Reykjavik");
			break;
		case 16:strcpy(s, "London");
			break;
		case 17:strcpy(s, "Porto");
			break;
		case 18:strcpy(s, "Barcelona");
			break;
		case 19:strcpy(s, "Dakar");
			break;
		case 20:strcpy(s, "Cape Town");
			break;
		case 21:strcpy(s, "Beira");
			break;
		case 22:strcpy(s, "Dar es Salaam");
			break;
		case 23:strcpy(s, "Colombo");
			break;
		case 24:strcpy(s, "Kochi");
			break;
		case 25:strcpy(s, "Port Said");
			break;
		case 26:strcpy(s, "Vladivostok");
			break;
		case 27:strcpy(s,"Osaka");
			break;
		case 28:strcpy(s, "Bangkok");
			break;
		case 29:strcpy(s, "Melbourne");
			break;
                default:strcpy(s, "No such station"); break;
	}
}
void stationsChoice(){
	int x,y,i;
	int t, st, choice;
	char station[18];
	char arrivalStation[18];
	char departureStation[18];
	char dateTmp[18];
	char *date;
	char *settleType;
	char success[] = "Booking has been successful!";
	char bookedSit[] = "Sit has been already booked";
	getmaxyx(stdscr, y, x);
    	stations = tui_new_shadowed_win(1, x/4, y-2, x/2, "Stations choice");
	for(i=1;i<30; i++){
		getStation(i, station);
		wprintw(stations->overlay,"%d. %s\n", i, station);
	}
	touchwin(panel_window(stations->panel));
	update_panels();
    	doupdate();
	t = tui_make_input("Arrival station number", 18);
        choice=strtol(t, 0, 10);
	getStation(choice, station);
	strcpy(arrivalStation, station);
	t = tui_make_input("Departure station number", 18);
	choice=strtol(t, 0, 10);
	getStation(choice, station);
	strcpy(departureStation, station);
	date = tui_make_input("Date: dd-mm-yyyy", 18);
	strcpy(dateTmp, date);
        dateTmp[10]='\0';
	touchwin(panel_window(stations->panel));
	update_panels();
    	doupdate();
	settleType = tui_make_input("Settle type(1,2 or 3)", 18);
	st=strtol(settleType, 0, 10);
	if(isFree(dateTmp, departureStation, arrivalStation, st) == 0)
	{
		tui_popup(bookedSit);
	}
	if(book(dateTmp, departureStation, arrivalStation, st) == 0)
	{
		tui_popup(bookedSit);
	}
	tui_popup(success);
	tui_del_win(stations);
}

typedef struct {
	short id;
	int x, y, z;
	mmask_t bstate;
} MEENT;

int main(void)
{
    if ( tui_init( ))
    {
        puts("error");
        return 1;
    }
    establish_connection();
    int32_t user_key = 0;
    char *test;
    wbkgd(stdscr, COLOR_PAIR(1));
    mvwprintw(stdscr,0,5,"Welcome to Breezy, the best solution for booking train tickets, F12 to exit");
    
    do
    {
	get_interfaces();
    cursed *but = tui_new_button(if_window->overlay,11,48,"Book", 4);
    cursed *but1 = tui_new_button(if_window->overlay,16,48,"Help", 4);
    cursed *but2 = tui_new_button(if_window->overlay,21,48,"Quit", 4);
        switch ( user_key )
        {
		case KEY_MOUSE:
			mvwprintw(stdscr, 10, 10, "");
			MEENT event;
			getmouse(&event);
			if(event.x >= 49 && event.x <= 54 && event.y >= 12 && event.y <= 15){
				tui_toggle_button(but);
				tui_del_win(but);
				tui_del_win(but1);
				tui_del_win(but2);
				tui_del_win(if_window);
				update_panels();
				doupdate();
				stationsChoice();
			}
			if(event.x >= 49 && event.x <= 54 && event.y >= 17 && event.y <= 20){
				tui_toggle_button(but1);
			}
			if(event.x >= 49 && event.x <= 54 && event.y >= 22 && event.y <= 25){
				tui_toggle_button(but2);
			}	

			break;
		case 98:
			tui_del_win(but);
			tui_del_win(but1);
			tui_del_win(but2);
			tui_del_win(if_window);
			
			update_panels();
			doupdate();
			stationsChoice();
			
			break;
		case 104:
			tui_toggle_button(but1);	
			break;
		case 113:
			goto exit;
			break;

        default:

        break;
        }
        update_panels( );
        doupdate( );
    }
    while (( user_key = getch( )) != KEY_F(12));
	exit:
    disconnect();
    if(stations)
        tui_del_win(stations);
    endwin( );
}



#include "file_writer.h"


int book_ticket(char** paths, int settle_type){
	/*
	 * book a place in every file
	 * of the specified route
	 * 0 on success
	 */	
	
	int f_d[PATH_MAX_LEN];
	int count = paths[0][0];
	int i = 1;
	int errnum = 0;
	
	//open each file for writing and write to files
	for(; i < count; i++){
		if((f_d[i] = open(paths[i], O_RDWR)) == -1)
			return errno;
		printf("%d\n", f_d[i]);
		//errnum = book_first_free(f_d[i], settle_type);			
		//char* item = (char*)malloc(10);
		//strcpy(item, "Asalle");
		//errnum = write(f_d[i], item, 10);
	}
	
	//if there were a mistake during writing to any of them -- discard changes
	//flush either way
	//if(!errnum)
		//for(i = 0; i < count; ++i)
			//if(fsync(f_d[i]) == -1)
				//return errno;
	
	//char c[10] = "HELLO!\n";
	//write(f_d[0], (char*)c, 10);	
			
	//for(i = 0; i < count; ++i)
		//if(close(f_d[0]) == -1)
			//return errno;		
	
	return 0;			
}

int book_first_free(int fd, int settle_type){
	/*
	 * book a place in a single file
	 * 0 on success
	 * +
	 */
	 
	char* sit = (char*)malloc(sizeof(char));
	int errnum = 0;
	while(read(fd, sit, sizeof(char)) > 0){
		if(atoi(sit) == settle_type){
			lseek(fd, sizeof(char), SEEK_CUR);
			snprintf(sit, sizeof(char), "%d", PLACE_BOOKED);
			if(write(fd, sit, sizeof(char)) == -1)
				return errno;
			else return 0;	
		}
	}
	return 1;
			
	
}

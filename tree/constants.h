// for bash interconnection
#define HEAD "BREEZY_ROUTE_HEADS"
#define BODY "BREEZY_BODY"


// for graph build
#define VERTICES_SZ 30
#define EDGES_SZ 900
#define MAX_INCIDENT_EDGES 50

//for everything
typedef struct Date{
	char minute;
	char hour;
    char day;
    char month;
    int year;    
}Date;

//for strings max length
#define PATH_MAX_LEN 255
#define MAX_ROUTE_LEN 30

//for writing to file
#define PLACE_BOOKED 0

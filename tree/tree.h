#include "constants.h"

#include <stdlib.h>
#include <dirent.h>
#include <string.h>

/*-------------- FILE STUFF INCLUDES --------------*/

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <assert.h>



typedef struct vertex_struct{
		char station_name[255];
		int incident_edges_iter;
		int incident_edges[MAX_INCIDENT_EDGES];
}vertex_t;

typedef struct edge_struct{
		int from_vertice_num;
		int to_vertice_num;
		char path_to_route_file[255];
		Date departure;
		Date arrival;
}edge_t;



typedef struct graph_struct{
int vertex_iter;
int edge_iter;
vertex_t vertices[VERTICES_SZ];
edge_t edges[EDGES_SZ];
}graph;

graph* main_graph;



void aux_print_graph(graph* route_graph);
int read_points(const char*, graph*);
int read_route_dir(DIR* route_dir, char* path, int path_length, graph*);
int init_tree();

int tree_find_route(Date departure_date, char* departure, char* arrival, int settle_type, char** paths);

Date* parse_date(char* date_str);

int book_ticket(char** paths, int settle_type);
int book_first_free(int filedescr, int settle_type);
int check_place_available(char** path, int);


//#define vert_it route_graph->vertex_iter
//#define edge_it route_graph->edge_iter
//#define redges_it route_graph->edges[route_graph->edge_iter]

#include "tree.h"

#include <stdio.h>

#define HEAD_STR_SZ 255
#define BODY_STR_SZ 255

#define xsize 3
#define ysize 5

int visited[VERTICES_SZ] = {0};
char valid[EDGES_SZ];

/*------------------- AUXILLARY USELESS PRINT FUNCS ----------------------------*/

void aux_print_graph(graph* route_graph){
	//printf("vertex: %d\t", vert_it);
	//printf("edge: %d\t", edge_it);
	int i = 0;
	puts("----\n");
	for(; i < route_graph->vertex_iter; ++i){
		printf("i = %d\n", i);
		printf("to vertice num: %d\t", route_graph->edges[i].to_vertice_num);
	}
	puts("\n");
	//printf("incident edges of [0] count: %d\n", route_graph->vertices[0].incident_edges_iter);
}

void aux_print_vertex(vertex_t v){
	printf(":%s:\n", v.station_name);
	printf("%d\n", v.incident_edges_iter);
	int i = 0;
	for(; i < v.incident_edges_iter; ++i)
		printf("%d\t", v.incident_edges[i]);
	puts("\n");
}

/*------------------- DEPTH-FIRST SEARCH ----------------------------*/

int depth_first(int vertice_number, char* station_name, char** paths){
	
	/*
	 *  
	 * I will never be able to get what is happening here next day
	 * 0 if success 
	 * 1 if not success
	 */
	 
	//if c is goal -- exit
	if(strcmp(main_graph->vertices[vertice_number].station_name, station_name) == 0 ){
		return 0; // this is it!
	}
					 
	//else mark in progress
	visited[vertice_number] = 1; //in progress	
	
	int i = 0;
	//for every incident n 
	for(; i < main_graph->vertices[vertice_number].incident_edges_iter; ++i){
				
		if(!valid[main_graph->vertices[vertice_number].incident_edges[i]] || main_graph->edges[main_graph->vertices[vertice_number].incident_edges[i]].to_vertice_num == vertice_number)
			continue;
		
		if(visited[main_graph->edges[main_graph->vertices[vertice_number].incident_edges[i]].to_vertice_num] == 0)				
			if(depth_first(main_graph->edges[main_graph->vertices[vertice_number].incident_edges[i]].to_vertice_num, station_name, paths) == 0){
				if(paths[0] != NULL){
					paths[0][0]++;
					char* path_aux = (char*)malloc(PATH_MAX_LEN);
					strcat(path_aux, main_graph->edges[main_graph->vertices[vertice_number].incident_edges[i]].path_to_route_file);
					strcat(path_aux, "/");
					strcat(path_aux, main_graph->vertices[main_graph->edges[main_graph->vertices[vertice_number].incident_edges[i]].to_vertice_num].station_name);
					strcpy(paths[paths[0][0]], path_aux);
				}
			return 0;	
		}
	}
	visited[vertice_number] = 2;
	return 1; // nothing found =(
}

/*------------------- DATE STUFF ----------------------------*/

Date* parse_date(char* date_str){
	/*
	 * parses a char* like 12-31-2014 to a Date struct
	 * return 0 otherwise
	 */
	char** datearr = (char**)malloc(xsize); //month, day, year
	int i = 0;
	for(; i < xsize; ++i)
		datearr[i] = (char*)malloc(ysize);
	
	//strtok must take []-style strings
	//doesn't work with char*
	char const_date_str[255];
	strcpy(const_date_str, date_str);
	char* token; 
		
	//init	
	datearr[0] = strtok(const_date_str, "-");
	//puts(datearr[0]);
	
	i = 1;
	while(i < 3){
		datearr[i] = strtok(NULL, "-");
		i++;
	}	
	Date* d  = (Date*)malloc(sizeof(Date));
	d->year = atoi(datearr[2]);
	d->month = atoi(datearr[1]);
	d->day = atoi(datearr[0]);
	
	return d;
}

int dtcmp(Date d1, Date d2){
	/*
	 * 
	 * Compare two dates
	 * 0 if ident
	 * 1 if not
	 */
	 
	if(d1.year != d2.year ||
		d1.month != d2.month ||
		d1.day != d2.day)
		return 1;
	return 0;
}

/*------------------- MAIN FIND FUNC ----------------------------*/

int tree_find_route(Date departure_date, char* departure, char* arrival, int settle_type, char** paths)
{
		graph aux_graph = *main_graph;
		
		memset(visited, 0, sizeof(visited));
		memset(valid, 1, EDGES_SZ);
		int i = 0, sum = 0;
		// throw away all the other days
		for(; i < aux_graph.edge_iter; ++i)
			if(dtcmp(aux_graph.edges[i].departure, departure_date))
				valid[i] = 0;
		
		for(i = 0; i < aux_graph.edge_iter; ++i)
			sum |= valid[i];
		
		if(sum == 0){
			return 4; //no such date
			
		}
		
		//find the departure station
		int the_station_exists = 0;
		for(i = 0; i < aux_graph.vertex_iter; ++i)
			if(strcmp(departure, aux_graph.vertices[i].station_name) == 0)
				the_station_exists = 1;
						
		if(!the_station_exists){
			puts("No such station");
			return 2; // no such departure station
		}
		
		the_station_exists = 0;	
		for(i = 0; i < aux_graph.vertex_iter; ++i)
			if(strcmp(arrival, aux_graph.vertices[i].station_name) == 0)
				the_station_exists = 1;			
					
		if(!the_station_exists)
			return 3; // no such arrival station
		
		if(depth_first(graph_vertices_count(main_graph, departure), arrival, paths) == 0)
			if(check_place_available(paths, settle_type) == 0){
				memset(visited, 0, VERTICES_SZ);
				memset(valid, 1, EDGES_SZ);
				return 0; //success
			}
		
				
		memset(visited, 0, VERTICES_SZ);
		memset(valid, 1, EDGES_SZ);
		return 1; //fail		
}



int graph_vertices_count(graph* route_graph, const char* item){
	/*
	 * returns the number of char* item in graph vertices arr
	 * like it does hashmap,
	 * if there is no such item it returns -1
	 */
	 
	 if(route_graph == NULL)
		return -1;
	 
	 int counter = 0;
	 
	 for(; counter < route_graph->vertex_iter; ++counter)
		if(strcmp(route_graph->vertices[counter].station_name, item) == 0)
			return counter;
		
	return -1;
}

/*------------------- GRAPH INIT FUNCS ----------------------------*/

int read_edges(char* path, char* filename, char* datestr, graph* route_graph){
	/*
	 * 
	 * this one reads all the station data and 
	 * creates the respective graph edges
	 * 
	 */	
	char* edgefile_path = (char*)malloc(PATH_MAX_LEN);
	strcat(edgefile_path, path);
	strcat(edgefile_path, "/");
	strcat(edgefile_path, filename);
	
	DIR* edgedir = opendir(edgefile_path);
	struct dirent* pre_edge = NULL;
	struct dirent* edge;
	
	while(edge = readdir(edgedir)){
		
		//initialize edges of route graph
		 if(strcmp(edge->d_name, ".") == 0 || strcmp(edge->d_name, "..") == 0)
			continue;
			
		route_graph->edges[route_graph->edge_iter].from_vertice_num = 
		pre_edge == NULL ? -1:graph_vertices_count(route_graph, pre_edge->d_name);
		
		route_graph->edges[route_graph->edge_iter].to_vertice_num = 
		edge == NULL?-1:graph_vertices_count(route_graph, edge->d_name);
		
		strcpy(route_graph->edges[route_graph->edge_iter].path_to_route_file, edgefile_path);
		
		route_graph->edges[route_graph->edge_iter].departure = 
		route_graph->edges[route_graph->edge_iter].arrival = *parse_date(datestr);
		
		if(pre_edge != NULL)
			route_graph->vertices[graph_vertices_count(route_graph, pre_edge->d_name)].
				incident_edges[route_graph->vertices[graph_vertices_count(route_graph, pre_edge->d_name)].
				incident_edges_iter++] = route_graph->edge_iter;
				
		//route_graph->vertices[graph_vertices_count(route_graph, edge->d_name)].
		//incident_edges[route_graph->vertices[graph_vertices_count(route_graph, edge->d_name)].
		//incident_edges_iter++] = route_graph->edge_iter;
		
		route_graph->edge_iter++;		
		pre_edge = edge;
	}	
	//aux_print_graph(route_graph);
	return 0;
	
}

int read_train_dir(DIR* train_dir, char* body_path, int body_path_length, graph* route_graph){
	/*
	 * this stranger goes over dir entries and prepares for every of them
	 * a full path to come and visit them
	 * 
	 */
	struct dirent* date;
	while(date = readdir(train_dir)){
		 if(strcmp(date->d_name, ".") == 0 || strcmp(date->d_name, "..") == 0)
			continue;
		
		//prepare full path
		char* path = (char*)malloc(PATH_MAX_LEN);
		strcat(path, body_path);
		strcat(path, "/");
		strcat(path, date->d_name);
		
		DIR* date_dir = opendir(path);
		if(date_dir == NULL)
			return 1;
		struct dirent* route_dir;
		
		while(route_dir = readdir(date_dir)){
		
			if(strcmp(route_dir->d_name, ".") == 0 || strcmp(route_dir->d_name, "..") == 0)
				continue;
						
			if(read_edges(path, route_dir->d_name, date->d_name, route_graph))
				return 2;
		}
	}
	
	return 0;
}

int read_points(const char* points_file_path, graph* route_graph){
	/* 
	 * this one reads all the station names and 
	 * creates the respective graph vertices
	 * 
	 */
	 
	int linenum = -1;
	char* point_name = (char*)malloc(HEAD_STR_SZ);
	FILE* points_file = fopen(points_file_path, "r");
	if(points_file == NULL)
		return 1;
	
	int i = 0;
	for(; i < VERTICES_SZ; ++i)
		route_graph->vertices[i].incident_edges_iter = 0;
	
	while(fgets(point_name, HEAD_STR_SZ, points_file))
	{
		linenum++;
		if(linenum % 3 != 0)
			continue;
			
		point_name[strlen(point_name)-1] = 0; // \n at the end of line added by echo 
		memset(route_graph->vertices[route_graph->vertex_iter].station_name, 0, 255);
		memset(route_graph->vertices[route_graph->vertex_iter].incident_edges, -1, EDGES_SZ);
		
		if(graph_vertices_count(route_graph, point_name) == -1)
		{
			route_graph->vertices[route_graph->vertex_iter].incident_edges_iter = 0;
			if(strcpy(route_graph->vertices[route_graph->vertex_iter].station_name, point_name) != NULL){
				route_graph->vertex_iter++;
			}
			else
				puts("=(");
		}
		
	}
	
	return 0;
}

int read_route_dir(DIR* route_dir, char* path, int path_length, graph* route_graph){
	/*
	 * this stranger goes over dir entries and prepares for every of them
	 * a full path to come and visit them
	 * 
	 */
	 
	struct dirent* points;
	while(points = readdir(route_dir)){
		 if(strcmp(points->d_name, ".") == 0 || strcmp(points->d_name, "..") == 0)
			continue;
				
		//prepare the full path
		char* points_path = (char*)malloc(PATH_MAX_LEN);
		strcat(points_path, path);
		strcat(points_path, "/");
		strcat(points_path, points->d_name);
		if(read_points(points_path, route_graph) != 0)
			return 1;
	}
}

int init_tree(){
		/*
		 * graph initialization
		 * 
		 */
		 
		graph* route_graph = (graph*)malloc(sizeof(graph));
		route_graph->vertex_iter = 0;
		route_graph->edge_iter = 0;
		
		char* head_path = (char*)malloc(HEAD_STR_SZ);
		char* body_path = (char*)malloc(BODY_STR_SZ);
		FILE* exports = fopen("/tmp/breezy/exports", "r");
		if(exports == NULL)
			return 3;
		head_path = fgets(head_path, HEAD_STR_SZ, exports);
		head_path[strlen(head_path)-1] = 0; // to remove the \n symbol added by echo
		body_path = fgets(body_path, BODY_STR_SZ, exports);
		body_path[strlen(body_path)-1] = 0; // to remove the \n symbol added by echo
		
		if(head_path == NULL || body_path == NULL){
			puts("Please, verify the /tmp/breezy/exports file contains exactly 2 lines");
			return 1;
		}
		DIR* route_dir = opendir(head_path);
		DIR* train_dir = opendir(body_path);
		if(route_dir == NULL || train_dir == NULL)
			return 2;	
		if(read_route_dir(route_dir, head_path, strlen(head_path), route_graph) != 0)
			return 4;
		if(read_train_dir(train_dir, body_path, strlen(body_path), route_graph) != 0)
			return 5;
		main_graph = route_graph;
		
	return 0;
}

/*--------------------------- FILE STUFF FUNCS ------------------------------*/

int check_place_available(char** path, int type){
	/*
	 * 0 -- found places available
	 * else -- no more places
	 */
	
	int fd, i = 1;
	int errnum;
				
	char* place = (char*)malloc(sizeof(char));
	char* place_we_need = (char*)malloc(sizeof(char));
	sprintf(place_we_need, "%d", type);
	if(path[0] != NULL){
		for(; i <= path[0][0]; ++i){
			fd = open(path[i], O_RDONLY);
			if(fd < 0)
				return errno;
			char available_flag = 0;
			while((errnum = read(fd, place, sizeof(char))) != 0){
				if(strcmp(place, place_we_need) == 0)
					available_flag = 1;
			}
			if(available_flag == 0){
				close(fd);
				puts("Nope");
				return 1;
			}
		}
	}
	else puts("NULL");
	
	close(fd);
	return 0;
}

int b_t(char** paths, int settle_type){
	int fd = 4;
	fd = open("/tmp/hello", O_WRONLY);
	assert(fd > 2);
	
	
}

int book_ticket(char** paths, int settle_type){
	/*
	 * book a place in every file
	 * of the specified route
	 * 0 on success
	 */	
	 
	int f_d[PATH_MAX_LEN];
	int count = paths[0][0];
	int i = 0;
	int errnum = 0;
	
	//open each file for writing and write to files
	for(; i < count; i++){
		puts(paths[i+1]);
		f_d[i] = open(paths[i+1], O_RDWR);
		if(f_d[i] < 0)
			return errno;
		
		errnum = book_first_free(f_d[i], settle_type);			
		
	}
	
	for(i = 0; i < count; ++i)
		if(close(f_d[i]) == -1)
			return errno;		
	
	return 0;			
}

int book_first_free(int fd, int settle_type){
	/*
	 * book a place in a single file
	 * 0 on success
	 * +
	 */
	 
	char* sit = (char*)malloc(sizeof(char));
	int errnum = 0;
	while(read(fd, sit, sizeof(char)) > 0){
		if(atoi(sit) == settle_type){
			lseek(fd, -sizeof(char), SEEK_CUR);
			sprintf(sit, "%d", PLACE_BOOKED);
			if(write(fd, sit, sizeof(char)) == -1)
				return errno;
			else return 0;	
		}
	}
	return 1;
			
	
}


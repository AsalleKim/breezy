//Commands
#define NEW_CONN_CMD    1
#define BOOK_CMD  2
#define IS_FREE_CMD 3
#define EXIT_CMD 4

//Consts
#define MSG_MAX 1024
#define CITYNAME_LENGTH 41
#define DATE_LENGTH 11

//Responses

//Common data types
struct sessions{
    mqd_t ctos;
    mqd_t stoc;
};

struct msgcontent{
    long mtype;
    int cmd;
    char data[MSG_MAX-sizeof(int)];
};

struct isfreeargs{
    long mtype;
    int cmd;
    char date[DATE_LENGTH];
    char ds[CITYNAME_LENGTH];
    char as[CITYNAME_LENGTH];
    int st;
    int answer;
};

struct initmsg{
    long mtype;
    int cmd;
    char answer[40];
};

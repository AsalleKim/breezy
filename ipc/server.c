#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include <fcntl.h>
#include "../common/commons.h"
#include "../server_storage/storage.h"

#define MAX_CLIENTS 50 //must not be lower than 3

void * handler(void * arg);
int isFreeUnpack(char *);
int bookUnpack(char *);
char** splitbyws(char *, int *);
void sig_handler(int);

pthread_mutex_t booklock;
int client_counter; 

int main(int argc, char** argv){    
    struct sessions s[MAX_CLIENTS];
    char qnamein[40]="/breezy";
    char qnameout[40];
    char tempname[40];
    char tempc[10];
    pthread_t *thread;
    struct initmsg msg;
    FILE *ipc_file;
    mqd_t queuectos, queuestoc, session;
    struct mq_attr attr;
    struct mq_attr sattr;
    
    attr.mq_flags = 0;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = sizeof(struct initmsg);
    attr.mq_curmsgs = 0;
    
    sattr.mq_flags = 0;
    sattr.mq_maxmsg = 10;
    sattr.mq_msgsize = sizeof(struct isfreeargs);
    sattr.mq_curmsgs = 0;
    
    client_counter=0;

    printf("Server started\n");    
   
    init_storage();
    if(argc==2)
        strcpy(qnamein, argv[1]);
    strcpy(tempname, qnamein);    
    strcpy(qnameout, qnamein);
    qnameout[1]++;
    
    printf("%s\n", qnamein);
    if(mq_unlink(qnamein)==0)
        printf("Old queuein removed\n");
    else
        fprintf(stderr, "%s\n", strerror(errno));
    
    printf("%s\n", qnameout);
    if(mq_unlink(qnameout)==0)
        printf("Old queueout removed\n");
    else
        fprintf(stderr, "%s\n", strerror(errno));   
    
    ipc_file=fopen("queue.name", "w+b");
    if(ipc_file==0){
        fprintf(stderr, "%s\n", strerror(errno));        
        exit(1);}    

    queuectos = mq_open(qnamein, O_CREAT | O_RDWR, 0666, &attr);
    if(queuectos==-1){
        fprintf(stderr, "%s\n", strerror(errno));        
        exit(1);}            
    
    queuestoc = mq_open(qnameout, O_CREAT | O_RDWR, 0666, &attr);
    if(queuestoc==-1){
        fprintf(stderr, "%s\n", strerror(errno));        
        exit(1);}

    fprintf(ipc_file, "%s", qnamein);   
    fclose(ipc_file);
	
    if (pthread_mutex_init(&booklock, NULL) != 0){
        fprintf(stderr, "Mutex init failed\n");
        exit(1);
    }
    
    if (signal(SIGINT, sig_handler) == SIG_ERR)
      fprintf(stderr, "Can't catch SIGINT\n");
    printf("Ready\n");
    
    while(1){
        if(mq_receive(queuectos, &msg, sizeof(struct initmsg), 0)==-1){
            fprintf(stderr, "%s\n", strerror(errno));
            exit(5);}              
        printf("Got a connection. Mtype is %ld\n", msg.mtype);
        if(msg.cmd != NEW_CONN_CMD)
            continue;
        client_counter++;
        if(client_counter>MAX_CLIENTS){                                                      
            client_counter--; 
            continue;  
        }
        
        sprintf(tempc, "%d", client_counter);
        strcpy(tempname, qnamein);
        strcat(tempname, tempc);
        printf("Tempname - %s\n", tempname);
        mq_unlink(tempname);
        session = mq_open(tempname, O_CREAT | O_RDWR, 0666, &sattr);
        if(session==-1){
            fprintf(stderr, "%s\n", strerror(errno));        
            exit(2);} 
        s[client_counter-1].ctos=session;
        
        tempname[1]++;
        mq_unlink(tempname);
        session = mq_open(tempname, O_CREAT | O_RDWR, 0666, &sattr);
        if(session==-1){
            fprintf(stderr, "%s\n", strerror(errno));        
            exit(2);} 
        s[client_counter-1].stoc=session;
        tempname[1]--;
        
        strcpy(msg.answer, tempname);
        if(mq_send(queuestoc, &msg, sizeof(struct initmsg), 1)==-1)
            fprintf(stderr, "%s\n", strerror(errno));
        
        thread=(pthread_t *)malloc(sizeof(pthread_t));
        if(thread==0)
            exit(2);                     
        pthread_create(&thread, NULL, handler, &s[client_counter-1]);                
    }
    
    exit(0);
}

void * handler(void * arg){      
    struct isfreeargs args;
    struct sessions session;

    session = * (struct sessions *) arg;
    printf("Hello from new thread!\n");
    while(1){
        if(mq_receive(session.ctos, &args, sizeof(struct isfreeargs), 0)==-1)
            fprintf(stderr, "%s\n", strerror(errno));
        printf("Got cmd - %d\n", args.cmd);
        switch(args.cmd){
            case BOOK_CMD:
                pthread_mutex_lock(&booklock);
                printf("Date  - %s, DS - %s, AS - %s, ST - %d\n", args.date, args.ds, args.as, args.st);
                args.answer=book(args.date, args.ds, args.as, args.st);
                if(mq_send(session.stoc, &args, sizeof(struct isfreeargs), 0)==-1)
                    fprintf(stderr, "%s\n", strerror(errno));
                pthread_mutex_unlock(&booklock);
                break;
            case IS_FREE_CMD:                
                printf("Date  - %s, DS - %s, AS - %s, ST - %d\n", args.date, args.ds, args.as, args.st);
                args.answer=isFree(args.date, args.ds, args.as, args.st);
                if(mq_send(session.stoc, &args, sizeof(struct isfreeargs), 0)==-1)
                    fprintf(stderr, "%s\n", strerror(errno));
                break;
            case EXIT_CMD:
                client_counter--;
                mq_close(session.ctos);
                mq_close(session.stoc);
                return;
                break;
            default:
                break;
        }
    }
}

void sig_handler(int signo){        
    printf("Deleting book mutex\n");
    pthread_mutex_destroy(&booklock);    
    exit(0);	
}




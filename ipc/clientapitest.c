#include "clientapi.h"
#include <stdio.h>
#include <stdlib.h>

int main(void){
    printf("Trying to connect...\n");
    if(establish_connection()==0)
        printf("Connection established\n");
    else 
        printf("Problem occured\n");
    sleep(1);
    printf("Sending isFree\n");
    isFree("10-20-2014", "Odessa", "Kiev", 2);    
    printf("Disconnecting\n");
    disconnect();
    exit(0);
}

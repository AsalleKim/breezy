int establish_connection(void);
int isFree(char* d, char* departure_station, char* arrival_station, int settle_type);
int book(char* d, char* departure_station, char* arrival_station, int settle_type);
void disconnect();

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <sys/types.h>
#include <unistd.h>
#include <mqueue.h>
#include <sys/stat.h>
#include <fcntl.h>         
#include "../common/commons.h"
#include "clientapi.h"

struct sessions s;

int establish_connection(){
    char key[40]; 
    mqd_t queuestoc, queuectos;
    struct initmsg msg;
    char buf[sizeof(struct initmsg)];
    FILE *ipc_file;
    
    msg.mtype=getpid();
    msg.cmd=NEW_CONN_CMD;
    
    ipc_file=fopen("queue.name", "r");

    if(ipc_file==0){
        fprintf(stderr, "No file was opened\n");
        return 1;}

    fscanf(ipc_file, "%s", key);

    //printf("Key is %s\n", key);

    queuectos=mq_open(key, O_RDWR);
    if(queuectos==-1){
        fprintf(stderr, "Can't open queue\n");
        return 2;}
    
    key[1]++;
    queuestoc=mq_open(key, O_RDWR);
    if(queuestoc==-1){
        fprintf(stderr, "Can't open queue\n");
        return 2;}

    memcpy(buf, &msg, sizeof(struct initmsg));
    if(mq_send(queuectos, buf, sizeof(struct initmsg), 0)==-1)
        fprintf(stderr, "%s\n", strerror(errno));  
    //printf("Mtype expected - %ld\n", msg.mtype);

  notminemsg:
    mq_receive(queuestoc, &msg, sizeof(struct initmsg), 0);
    if(msg.mtype!=getpid())
        goto notminemsg;
    strcpy(key, msg.answer);
    //printf("Got a session key %s\n", key);

    if((s.ctos=mq_open(key, O_RDWR))==-1)
        return 3;
        
    key[1]++;
    if((s.stoc=mq_open(key, O_RDWR))==-1)
        return 4;  
    return 0;
}

int isFree(char* date, char* departure_station, char* arrival_station, int settle_type){    
    struct isfreeargs msg;
    strcpy(msg.date, date);
    strcpy(msg.ds, departure_station);    
    strcpy(msg.as, arrival_station);    
    msg.st=settle_type;
    msg.cmd=IS_FREE_CMD;
    if(mq_send(s.ctos, &msg, sizeof(struct isfreeargs), 0)==-1)
        fprintf(stderr, "%s\n", strerror(errno));
    if(mq_receive(s.stoc, &msg, sizeof(struct isfreeargs), 0)==-1)
        fprintf(stderr, "%s\n", strerror(errno));
    return msg.answer;
}

int book(char* date, char* departure_station, char* arrival_station, int settle_type){    
    struct isfreeargs msg;
    strcpy(msg.date, date);
    strcpy(msg.ds, departure_station);    
    strcpy(msg.as, arrival_station);    
    msg.st=settle_type;
    msg.cmd=BOOK_CMD;
    if(mq_send(s.ctos, &msg, sizeof(struct isfreeargs), 0)==-1)
        fprintf(stderr, "%s\n", strerror(errno));
    mq_receive(s.stoc, &msg, sizeof(struct isfreeargs), 0);      
    return msg.answer;
}

void disconnect(){
    struct isfreeargs msg;    
    msg.cmd=EXIT_CMD;
    if(mq_send(s.ctos, &msg, sizeof(struct isfreeargs), 0)==-1)
        fprintf(stderr, "%s\n", strerror(errno));
}




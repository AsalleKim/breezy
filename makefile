
		objects = main_test.o storage.o tree.o
		
breezy : $(objects)
		 gcc -g $(objects) -o breezy
		 
storage.o : server_storage/storage.c server_storage/storage.h
			gcc -c -g server_storage/storage.c
			
tree.o : tree/tree.c tree/tree.h 
			gcc -c -g tree/tree.c
						
main_test.o : main_test.c tree/tree.h server_storage/storage.h
			gcc -c -g main_test.c

#objects = filewriter_test.o tree.o file_writer.o

#filetest : $(objects)
			#gcc -g $(objects) -o filetest

#tree.o : tree/tree.c tree/tree.h 
			#gcc -c -g tree/tree.c
	
#file_writer.o : tree/file_writer.c tree/file_writer.h
				#gcc -c -g tree/file_writer.c
				
#filewriter_test.o : filewriter_test.c tree/file_writer.c tree/tree.h 
				#gcc -c -g filewriter_test.c	
						
#clean : 
		#rm *.o

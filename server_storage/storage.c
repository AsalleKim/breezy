#include "storage.h"


int init_storage()
{
		//system("../input_generator/genHead res/stations");
		//system("../input_generator/genBody");
		
		if(init_tree())
			return 1;
		return 0;
}



int isFree(char* date, char* departure_station, char* arrival_station, int settle_type)
{
		Date d = *parse_date(date);
		
		char** f_paths = (char**)malloc(MAX_ROUTE_LEN * PATH_MAX_LEN);
		int i = 0;
		for(; i < MAX_ROUTE_LEN; ++i)
			f_paths[i] = (char*)malloc(PATH_MAX_LEN);
		//memset(f_paths, 0, MAX_ROUTE_LEN * PATH_MAX_LEN);
		//f_paths[0][0] = 0;
		
		int errnum = 0;
		int a;
		if(a = tree_find_route(d, departure_station, arrival_station, settle_type, f_paths)){
			printf("search error = %d/n", a);
			return 0;
			
		}
		
			
		// no such way from station to station in specified date and settle type
		return 1;		
}




int book(char* date, char* departure_station, char* arrival_station, int settle_type)
{		
	/*
	 * This functions books a first free place if route exists
	 * f_paths contains 
	 * [0] 	size of array n in first byte of char[]
	 * [1]	paths    
	 *  .	to
	 *  .	n
	 *  .	edge
	 * [n]	files
	 * 
	 */
		char** f_paths = (char**)malloc(MAX_ROUTE_LEN * PATH_MAX_LEN);
		int i = 0;
		for(; i < MAX_ROUTE_LEN; ++i)
			f_paths[i] = (char*)malloc(PATH_MAX_LEN);
		//memset(f_paths, 0, MAX_ROUTE_LEN * PATH_MAX_LEN);
		int errnum = 0;
			
		if(errnum = tree_find_route(*parse_date(date), departure_station, arrival_station, settle_type, f_paths)){
			printf("tree find route error = %d ", errnum);
			return 1;//fail
		}
		
		int a = 0;
		if(a = book_ticket(f_paths, settle_type)){
			return a;
		}		
		return 0;
		//success

}



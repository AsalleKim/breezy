#include <stdio.h>
#include "../tree/tree.h"



int init_storage();
int isFree(char* date, char* departure_station, char* arrival_station, int settle_type);
int book(char* date, char* departure_station, char* arrival_station, int settle_type);

